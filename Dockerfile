FROM node:lts-alpine3.13

COPY *.json ./
COPY yarn.lock ./
COPY src ./src

RUN yarn install && yarn build && cp -r ./src/proto ./build

EXPOSE 8080
EXPOSE 50051

CMD ["node", "build/index.js"]
