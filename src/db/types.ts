import { User, UserSession, UserProfile } from '../types/user';

export interface IUserRepository {
  saveUser: (user: User) => Promise<void>;
  findUserByEmail: (email: string) => Promise<User | undefined>;
  findUserByUsername: (username: string) => Promise<User | undefined>;
  findUserById: (id: string) => Promise<User | undefined>;
  findUsersByGroupId: (groupId: string) => Promise<string[]>;
}

export interface ISessionRepository {
  saveSession(session: UserSession): Promise<void>;
  findSession(sessionId: string): Promise<UserSession | undefined>;
}
