import { Firestore as FirestoreDB, Timestamp } from '@google-cloud/firestore';
import { User, UserSession } from '../types/user';
import { checkAndGetEnv } from '../utils/utils';
import { ISessionRepository } from './types';

export interface FirestoreUserSession {
  id: string;
  userId: string;
  username: string;
  fullName: string;
  validUntil: Timestamp;
}

export class Firestore implements ISessionRepository {
  private firestore: FirestoreDB;

  constructor() {
    try {
      this.firestore = new FirestoreDB({ keyFilename: checkAndGetEnv('FIRESTORE') });
    } catch (e) {
      console.error('Failed to connect to Firestore: ', e);
      throw e;
    }
  }

  public async saveSession(session: UserSession): Promise<void> {
    const sessionDocument = this.firestore.doc(`users/${session.id}`);
    await sessionDocument.set({
      id: session.id,
      userId: session.userId,
      username: session.username,
      fullName: session.fullName,
      validUntil: session.validUntil,
    });
  }

  public async findSession(sessionId: string): Promise<UserSession | undefined> {
    const sessionDocument = this.firestore.doc(`users/${sessionId}`);
    const response = await sessionDocument.get();
    if (response.exists) {
      const session = response.data() as FirestoreUserSession;
      return { ...session, validUntil: session.validUntil.toDate() } as UserSession;
    }
    return undefined;
  }
}
