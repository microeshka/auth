import { FastifyPluginCallback } from 'fastify';

export const healthCheckPlugin: FastifyPluginCallback<Record<string, never>> = (fastify, options, done) => {
  fastify.get('/auth/health', (request, response) => {
    // console.log('a1: health check');
    response.code(200).send('ok');
  });

  fastify.get('/group/health', (request, response) => {
    // console.log('a4: health check.');
    response.code(200).send('ok');
  });

  fastify.get('/healthz', (request, response) => {
    // console.log('a2: health check');
    response.code(200).send('ok');
  });

  fastify.get('/', (request, response) => {
    // console.log('a3: health check');
    response.code(200).send('ok');
  });

  done();
};
