export interface UserBase {
  id: string;
  email: string;
  fullName: string;
  username: string;
}

export interface User extends UserBase {
  password?: string;
}

export interface UserSignUp {
  username: string;
  email: string;
  fullName: string;
  password: string;
}

export interface UserSession {
  id: string;
  userId: string;
  username: string;
  fullName: string;
  validUntil: Date;
}

export interface Credentials {
  username: string;
  password: string;
}

export interface AfterLogin {
  id: string;
  fullName: string;
  email: string;
  username: string;
  sessionId?: string;
}

export interface GroupShort {
  id: string;
  name: string;
}

export interface QueueShort {
  id: string;
  title: string;
}

export interface SubjectShort {
  id: string;
  title: string;
}

export interface UserProfile extends UserBase {
  roles: string[];
  groupsInCommon: GroupShort[];
  authoredSubjects: SubjectShort[];
  authoredQueues: QueueShort[];
}
