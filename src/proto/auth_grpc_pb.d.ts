// package: auth
// file: auth.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "grpc";
import * as auth_pb from "./auth_pb";

interface IAuthService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    login: IAuthService_Ilogin;
    register: IAuthService_Iregister;
    findSession: IAuthService_IfindSession;
    findUser: IAuthService_IfindUser;
}

interface IAuthService_Ilogin extends grpc.MethodDefinition<auth_pb.LoginRequest, auth_pb.LoginResponse> {
    path: "/auth.Auth/login";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<auth_pb.LoginRequest>;
    requestDeserialize: grpc.deserialize<auth_pb.LoginRequest>;
    responseSerialize: grpc.serialize<auth_pb.LoginResponse>;
    responseDeserialize: grpc.deserialize<auth_pb.LoginResponse>;
}
interface IAuthService_Iregister extends grpc.MethodDefinition<auth_pb.RegisterRequest, auth_pb.RegisterResponse> {
    path: "/auth.Auth/register";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<auth_pb.RegisterRequest>;
    requestDeserialize: grpc.deserialize<auth_pb.RegisterRequest>;
    responseSerialize: grpc.serialize<auth_pb.RegisterResponse>;
    responseDeserialize: grpc.deserialize<auth_pb.RegisterResponse>;
}
interface IAuthService_IfindSession extends grpc.MethodDefinition<auth_pb.FindSessionRequest, auth_pb.Session> {
    path: "/auth.Auth/findSession";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<auth_pb.FindSessionRequest>;
    requestDeserialize: grpc.deserialize<auth_pb.FindSessionRequest>;
    responseSerialize: grpc.serialize<auth_pb.Session>;
    responseDeserialize: grpc.deserialize<auth_pb.Session>;
}
interface IAuthService_IfindUser extends grpc.MethodDefinition<auth_pb.FindUserRequest, auth_pb.User> {
    path: "/auth.Auth/findUser";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<auth_pb.FindUserRequest>;
    requestDeserialize: grpc.deserialize<auth_pb.FindUserRequest>;
    responseSerialize: grpc.serialize<auth_pb.User>;
    responseDeserialize: grpc.deserialize<auth_pb.User>;
}

export const AuthService: IAuthService;

export interface IAuthServer {
    login: grpc.handleUnaryCall<auth_pb.LoginRequest, auth_pb.LoginResponse>;
    register: grpc.handleUnaryCall<auth_pb.RegisterRequest, auth_pb.RegisterResponse>;
    findSession: grpc.handleUnaryCall<auth_pb.FindSessionRequest, auth_pb.Session>;
    findUser: grpc.handleUnaryCall<auth_pb.FindUserRequest, auth_pb.User>;
}

export interface IAuthClient {
    login(request: auth_pb.LoginRequest, callback: (error: grpc.ServiceError | null, response: auth_pb.LoginResponse) => void): grpc.ClientUnaryCall;
    login(request: auth_pb.LoginRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.LoginResponse) => void): grpc.ClientUnaryCall;
    login(request: auth_pb.LoginRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.LoginResponse) => void): grpc.ClientUnaryCall;
    register(request: auth_pb.RegisterRequest, callback: (error: grpc.ServiceError | null, response: auth_pb.RegisterResponse) => void): grpc.ClientUnaryCall;
    register(request: auth_pb.RegisterRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.RegisterResponse) => void): grpc.ClientUnaryCall;
    register(request: auth_pb.RegisterRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.RegisterResponse) => void): grpc.ClientUnaryCall;
    findSession(request: auth_pb.FindSessionRequest, callback: (error: grpc.ServiceError | null, response: auth_pb.Session) => void): grpc.ClientUnaryCall;
    findSession(request: auth_pb.FindSessionRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Session) => void): grpc.ClientUnaryCall;
    findSession(request: auth_pb.FindSessionRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Session) => void): grpc.ClientUnaryCall;
    findUser(request: auth_pb.FindUserRequest, callback: (error: grpc.ServiceError | null, response: auth_pb.User) => void): grpc.ClientUnaryCall;
    findUser(request: auth_pb.FindUserRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.User) => void): grpc.ClientUnaryCall;
    findUser(request: auth_pb.FindUserRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.User) => void): grpc.ClientUnaryCall;
}

export class AuthClient extends grpc.Client implements IAuthClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public login(request: auth_pb.LoginRequest, callback: (error: grpc.ServiceError | null, response: auth_pb.LoginResponse) => void): grpc.ClientUnaryCall;
    public login(request: auth_pb.LoginRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.LoginResponse) => void): grpc.ClientUnaryCall;
    public login(request: auth_pb.LoginRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.LoginResponse) => void): grpc.ClientUnaryCall;
    public register(request: auth_pb.RegisterRequest, callback: (error: grpc.ServiceError | null, response: auth_pb.RegisterResponse) => void): grpc.ClientUnaryCall;
    public register(request: auth_pb.RegisterRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.RegisterResponse) => void): grpc.ClientUnaryCall;
    public register(request: auth_pb.RegisterRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.RegisterResponse) => void): grpc.ClientUnaryCall;
    public findSession(request: auth_pb.FindSessionRequest, callback: (error: grpc.ServiceError | null, response: auth_pb.Session) => void): grpc.ClientUnaryCall;
    public findSession(request: auth_pb.FindSessionRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Session) => void): grpc.ClientUnaryCall;
    public findSession(request: auth_pb.FindSessionRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Session) => void): grpc.ClientUnaryCall;
    public findUser(request: auth_pb.FindUserRequest, callback: (error: grpc.ServiceError | null, response: auth_pb.User) => void): grpc.ClientUnaryCall;
    public findUser(request: auth_pb.FindUserRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.User) => void): grpc.ClientUnaryCall;
    public findUser(request: auth_pb.FindUserRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.User) => void): grpc.ClientUnaryCall;
}
