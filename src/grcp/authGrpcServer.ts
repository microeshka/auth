import { IAuthServer } from '../proto/auth_grpc_pb';
import { LoginRequest, LoginResponse, RegisterRequest, RegisterResponse, FindSessionRequest, Session, FindUserRequest, User } from '../proto/auth_pb';
import * as grpc from 'grpc';
import { Credentials, UserSignUp } from '../types/user';
import { errorToGrpcError } from '../utils/utils';
import { IAuthService as AuthService } from '../services/authService';
import { IUserService } from '../services/userService';

export class AuthServer implements IAuthServer {
  authService: AuthService;
  userService: IUserService;

  constructor(authService: AuthService, userService: IUserService) {
    this.authService = authService;
    this.userService = userService;
  }

  async findUser(call: grpc.ServerUnaryCall<FindUserRequest>, callback: grpc.sendUnaryData<User>): Promise<void> {
    try {
      const user = await this.userService.findUserById(call.request.getId());
      const response = new User();
      response.setId(user.id);
      response.setEmail(user.email);
      response.setFullName(user.fullName);
      response.setUsername(user.username);
      callback(null, response);
    } catch (err) {
      console.error(err);
      callback(errorToGrpcError(err), null);
    }
  }

  async login(call: grpc.ServerUnaryCall<LoginRequest>, callback: grpc.sendUnaryData<LoginResponse>): Promise<void> {
    try {
      const res = await this.authService.login(call.request.toObject() as Credentials);
      const response = new LoginResponse;
      response.setId(res.id);
      response.setUsername(res.username);
      response.setFullName(res.fullName);
      response.setEmail(res.email);
      response.setSessionId(res.id);
      callback(null, response);
    } catch (err) {
      console.error(err);
      callback(errorToGrpcError(err), null);
    }
  }

  async register(call: grpc.ServerUnaryCall<RegisterRequest>, callback: grpc.sendUnaryData<RegisterResponse>): Promise<void> {
    try {
      await this.authService.register(call.request.toObject() as UserSignUp);
      const response = new RegisterResponse;
      response.setMessage('Register success. You can login now');
      callback(null, response);
    } catch (err) {
      console.error(err);
      callback(errorToGrpcError(err), null);
    }
  }

  async findSession(call: grpc.ServerUnaryCall<FindSessionRequest>, callback: grpc.sendUnaryData<Session>): Promise<void> {
    try {
      const session = await this.authService.findSession(call.request.getId());
      const protoSession = new Session();
      protoSession.setId(session.id);
      protoSession.setUserId(session.userId);
      protoSession.setUsername(session.username);
      protoSession.setFullName(session.fullName);
      protoSession.setValidUntil(session.validUntil.getTime());
      callback(null, protoSession);
    } catch (err) {
      console.error(err);
      callback(errorToGrpcError(err), null);
    }
  }
}
