import { comparePasswords, hashPassword } from '../crypto/utils';
import { IUserRepository, ISessionRepository } from '../db/types';
import { AfterLogin, Credentials, User, UserSession, UserSignUp } from '../types/user';
import { badCredentials } from './errors';
import * as uuid from 'uuid';
import { validateEmail, validateFullName, validatePassword, validateUsername } from '../utils/validation';

export interface IAuthService {
  login(credentials: Credentials): Promise<AfterLogin>;
  register(userData: UserSignUp): Promise<void>;
  findSession(id: string): Promise<UserSession>;
}

export class AuthService implements IAuthService {
  database: IUserRepository;
  sessionStore: ISessionRepository;

  constructor(database: IUserRepository, sessionStore: ISessionRepository) {
    this.database = database;
    this.sessionStore = sessionStore;
  }

  async login(credentials: Credentials): Promise<AfterLogin> {
    const user = await this.database.findUserByUsername(credentials.username);
    if (!user) {
      throw badCredentials();
    }
    if (user?.password && await comparePasswords(credentials.password, user?.password)) {
      // 7 200 000 === 2 hours
      const validUntil = new Date();
      validUntil.setTime(validUntil.getTime() + 7200000);
      console.log(user);
      const session: UserSession = {
        id: uuid.v4(),
        userId: user.id,
        username: user.username,
        fullName: user.fullName,
        validUntil: validUntil,
      };
      console.log('start save session');
      await this.sessionStore.saveSession(session);
      console.log('end save session');
      return {
        id: user.id,
        fullName: user.fullName,
        email: user.email,
        username: user.username,
        sessionId: session.id,
      } as AfterLogin;
    }
    throw badCredentials();
  }

  async register(userData: UserSignUp): Promise<void> {
    await validateEmail(this.database, userData.email);
    await validateUsername(this.database, userData.username);
    validateFullName(userData.fullName);
    validatePassword(userData.password);

    const userId = uuid.v4();
    userData.password = await hashPassword(userData.password);

    const user: User = {
      ...userData,
      id: userId,
    };
    console.log(user);

    await this.database.saveUser(user);
  }

  async findSession(id: string): Promise<UserSession> {
    const session = await this.sessionStore.findSession(id);
    if (!session) {
      throw { status: 404, errorMessage: `Session with id=${id} not found!` };
    }
    return session;
  }
}
