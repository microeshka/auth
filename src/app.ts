import { Firestore } from './db/firestore';
import { Postgres } from './db/postgres';
import { IUserRepository, ISessionRepository } from './db/types';
import { IAuthService, AuthService } from './services/authService';
import * as grpc from 'grpc';
import { AuthServer as GrpcAuthServer } from './grcp/authGrpcServer';
import { IAuthServer as IGrpcAuthServer, AuthService as GrpcAuthService } from './proto/auth_grpc_pb';
import { IUserService, UserService } from './services/userService';
import { checkAndGetEnv } from './utils/utils';
import { createFastifyServer } from './config/fastifyServer';

export class Application {
  private database: IUserRepository;
  private sessionStore: ISessionRepository;
  private authService: IAuthService;
  private userService: IUserService;
  private grpcServer: grpc.Server;
  private fastifyServer: any;

  constructor() {
    this.database = new Postgres({
      max: 20,
      idleTimeoutMillis: 10000,
      connectionTimeoutMillis: 2000,
      connectionString: checkAndGetEnv('MAIN_DB_CONNECTION_STRING'),
    });
    this.sessionStore = new Firestore();
    this.authService = new AuthService(this.database, this.sessionStore);
    this.userService = new UserService(this.database);
    this.grpcServer = new grpc.Server();
    this.grpcServer.addService<IGrpcAuthServer>(GrpcAuthService, new GrpcAuthServer(this.authService, this.userService));
    this.grpcServer.bind(`0.0.0.0:${checkAndGetEnv('GRPC_SERVER_PORT')}`, grpc.ServerCredentials.createInsecure());
    this.fastifyServer = createFastifyServer(this.userService, this.authService);
  }

  public startGrpcServer() {
    this.grpcServer.start();
    console.log(`Grpc server started on port ${checkAndGetEnv('GRPC_SERVER_PORT')}`);
  }

  public startFastifyServer() {
    const port = checkAndGetEnv('HTTP_SERVER_PORT');
    this.fastifyServer.listen({ port }).then(() =>
      console.log(`App started! PORT: ${port}`),
    );
  }
}
