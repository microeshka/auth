import * as grpc from 'grpc';

export class GrpcCallError extends Error {
    status: number;
    errorMessage: string;
    constructor(error: grpc.ServiceError) {
      super();
      this.status = 501;
      this.errorMessage = error.details || '';
    }
}
