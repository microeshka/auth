import { status, ServiceError, Metadata } from 'grpc';

export const checkAndGetEnv = (envName: string, errMsg?: string): string => {
  const envVar = process.env[envName];
  if (envVar === undefined) {
    const error = new Error(errMsg || `${envName} not found in environment variables`);
    console.error(error);
    throw error;
  }
  return envVar;
};

const MAX_RETRIES = 5;

const waitForRetryTime = async (): Promise<void> =>
  new Promise((r) => setTimeout(r, 2000));

export const retry = async <T>(callback: () => Promise<T | undefined>, errorMsg: string): Promise<void> => {
  const i = 0;
  for (let i = 0; i < MAX_RETRIES; i++) {
    try {
      await callback();
      return;
    } catch (e) {
      console.error(`${errorMsg}. ${i + 1} try`, e);
      console.log('Retry to save user');
      await waitForRetryTime();
    }
  }
  throw new Error(errorMsg);
};

export const parseCookies = (cookie: string | undefined): Map<string, string> => {
  const res = new Map<string, string>();
  console.log({ cookie });
  if (!cookie) {
    return res;
  }
  for (const c of cookie.split(';')) {
    const [name, value] = c.split('=');
    res.set(name, value);
  }
  return res;
};

export const statusCodetoGrpcStatus = (code: number) => {
  switch (code) {
    case 404: return status.NOT_FOUND;
    case 400: return status.INVALID_ARGUMENT;
    case 500: return status.INTERNAL;
    case 401: return status.UNAUTHENTICATED;
    case 403: return status.PERMISSION_DENIED;
    default: return status.UNKNOWN;
  }
};

export const errorToGrpcError = (err: any): ServiceError => {
  const httpStatus = err.status || 500;
  const metadata = new Metadata();
  metadata.add('status', httpStatus + '');
  return { code: statusCodetoGrpcStatus(httpStatus), details: err.errorMessage || 'Internal server error', metadata } as ServiceError;
};
