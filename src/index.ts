import fastifyCookie from 'fastify-cookie';
import { Application } from './app';

const app = new Application();
app.startGrpcServer();
app.startFastifyServer();
