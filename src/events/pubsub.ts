import { PubSub } from '@google-cloud/pubsub';
import { checkAndGetEnv } from '../utils/utils';
import WebSocket = require('ws');
import { getConnections, getConnectionsMap } from '../ws/init';
import { IUserRepository } from '../db/types';

const pubSubClient = new PubSub({ keyFilename: checkAndGetEnv('PUBSUB_APPLICATION_CREDENTIALS') });

const topicName = 'notifications';

export interface Notification {
  type: string;
  senderId: string;
  message?: string;
  data: any;
}

export const receiver = async (database: IUserRepository) => {
  const topik = await pubSubClient.topic(topicName);
  const subsciprion = await topik.subscription('statistic-subscription');
  subsciprion.on('message', (message) => {
    const notification = JSON.parse(message?.data?.toString()) as Notification;
    console.log(notification);
    if (notification.data.groupId) {
      sendToAllInGroup(notification, database);
    } else {
      sendToAll(notification);
    }
    message.ack();
  });
  subsciprion.on('error', (error) => {
    console.log('Error:');
    console.log(error);
  });
};

const sendToAll = (notification: Notification) =>
  getConnections().forEach((ws) => ws.send(JSON.stringify(notification)));

const sendToAllInGroup = async (notification: Notification, database: IUserRepository) => {
  const users = await database.findUsersByGroupId(notification.data.groupId);
  console.log(`users in group: ${JSON.stringify(users)}`);
  const connections = getConnectionsMap();
  users.forEach((userId) => {
    const ws = connections.get(userId);
    console.log(`send to user: ${userId}. sender: ${notification.senderId}`);
    if (userId !== notification.senderId && ws) {
      ws.send(JSON.stringify(notification));
    }
  });
};
