import WebSocket = require('ws');
import * as uuid from 'uuid';
import { Server } from 'http';
import { parseCookies } from '../utils/utils';
import { ISessionRepository } from '../db/types';

const connections = new Map<string, any>();

export const getConnections = () => Array.from(connections.values());
export const getConnectionsMap = () => connections;

export const initWebSocketServer = (server: Server, sessionStore: ISessionRepository) => {
  console.log(WebSocket);
  const wss = new WebSocket.Server({ noServer: true });

  server.on('upgrade', (request, socket, head) => {
    console.log('in upgrade:');
    const sessionId = parseCookies(request.headers['cookie']).get('SessionCookies');
    if (!sessionId) {
      socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
      socket.destroy();
      return;
    }
    sessionStore.findSession(sessionId).then((session) => {
      if (!session) {
        socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
        socket.destroy();
        return;
      }
      wss.handleUpgrade(request, socket, head, (ws) => {
        connections.set(session.userId, ws);
        wss.emit('connection', ws, request);
      });
    });
  });

  wss.on('connection', async (ws: any, request: any) => {
    console.log('new ws connection');
    const id = uuid.v4();
    ws.on('message', (message: any) => {
      console.log('new message:');
      console.log(message);
      ws.send('message saved!.');
    });
    ws.on('close', () => {
      console.log(`connection ${id} closed`);
      connections.delete(id);
    });
    ws.on('error', (err: any) => {
      console.log(`err with connection ${id} :`);
      console.log(JSON.stringify(err));
    });
    console.log('------------------');
    ws.send(`${id} connected!`);
    // connections.set(id, ws);
    console.log('connected!');
  });
  console.log('web socker server configured');
};
